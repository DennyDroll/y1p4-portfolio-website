$(document).ready(onReady);

function onReady()
{
    buttons();
}

function buttons()
{
    $('#home').click(function()
    {
        window.location.href = "/index.html";
    });
    $('#prevWork').click(function()
    {
        window.location.href = "/pages/prevWork.html";
    });
    $('#cv').click(function()
    {
        window.location.href = "/pages/cv.html";
    });
    $('#links').click(function()
    {
        window.location.href = "/pages/links.html";
    });
    $('#contact').click(function()
    {
        window.location.href = "/pages/contact.html";
    });
}